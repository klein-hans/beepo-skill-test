<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::prefix('tasks')->group(function () {
        Route::get('/', 'App\Http\Controllers\TasksController@index')->name('tasks.index');
        Route::get('/create', 'App\Http\Controllers\TasksController@create')->name('tasks.create');
        Route::post('/tasks', 'App\Http\Controllers\TasksControlle@store')->name('tasks.store');
        Route::get('/tasks/{task}/edit', 'App\Http\Controllers\TasksControlle@edit')->name('tasks.edit');
        Route::patch('/tasks/{task}', 'App\Http\Controllers\TasksControlle@update')->name('tasks.update');
        Route::delete('/tasks/{task}', 'App\Http\Controllers\TasksControlle@destroy')->name('tasks.destroy');
    });
});

require __DIR__.'/auth.php';
